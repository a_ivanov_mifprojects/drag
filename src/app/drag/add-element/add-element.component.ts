import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-element',
  templateUrl: './add-element.component.html',
  styleUrls: ['./add-element.component.scss']
})
export class AddElementComponent {

  @Output() addlement = new EventEmitter();

  elementsForm: FormGroup;
  showForm: boolean;

  constructor(private formBuilder: FormBuilder) {
    this.elementsForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(1)]]
      }
    );
  }

  isShowForm(state: boolean) {
    this.showForm = state;
  }

  onSubmit(form: FormGroup) {
    this.isShowForm(false);
    this.addlement.emit(form.value.name);
    this.elementsForm.reset();
  }
}

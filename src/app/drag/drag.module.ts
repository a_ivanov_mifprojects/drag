import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddElementComponent} from './add-element/add-element.component';
import {DragComponent} from './drag/drag.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ClickOutsideDirective} from './click-outside.directive';
import {DraggableDirective} from './draggable.directive';

@NgModule({
  declarations: [
    AddElementComponent,
    DragComponent,
    ClickOutsideDirective,
    DraggableDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    DragComponent
  ]
})
export class DragModule {
}

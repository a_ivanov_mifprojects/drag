import {Directive, ElementRef, HostBinding, OnInit} from '@angular/core';
import {fromEvent} from 'rxjs';
import {mergeMap, repeat, takeUntil} from 'rxjs/operators';
import {SafeStyle} from '@angular/platform-browser';

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective implements OnInit {
  elementPosition = [0, 0];

  @HostBinding('style.transform') get style(): SafeStyle {
    return `translate3d(${this.elementPosition[0]}px, ${this.elementPosition[1]}px, 0)`;
  }

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    fromEvent<MouseEvent>(this.elementRef.nativeElement, 'mousedown').pipe(
      mergeMap(() => fromEvent<MouseEvent>(document, 'mousemove')),
      takeUntil(fromEvent<MouseEvent>(document, 'mouseup')),
      repeat())
      .subscribe(event => {
        this.elementPosition[0] += event.movementX;
        this.elementPosition[1] += event.movementY;
      });
  }
}

import {Component} from '@angular/core';

@Component({
  selector: 'app-drag',
  templateUrl: './drag.component.html',
  styleUrls: ['./drag.component.scss']
})
export class DragComponent {

  elements: string[] = [];

  constructor() {
  }

  addElement(elementName: string) {
    this.elements.push(elementName);
  }

  deleteElement(id: number) {
    this.elements.splice(id, 1);
  }
}
